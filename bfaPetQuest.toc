## Interface: 80300
## Version: 1.1.4
## Title: BFA Pet Quests
## Author: Thomas Bemme
## Notes: Checks for completed BFA pet quest with slash commands /petdone or /pd
bfaPetQuest.lua