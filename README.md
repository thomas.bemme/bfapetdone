Shows completed BfA pet quests via /petdone or /pd

* Use /petdone or /pd to show incomplete pet quests in Nazjatar and Mechagon
* Use /petdone c or /pd c for list of all pet quests
* Click on pet names to add coordinates to TomTom


https://www.curseforge.com/wow/addons/bfapetdone
